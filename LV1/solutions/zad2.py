import sys
x=-1.1

while(x<0 or x>1):
     try:
        x = float(input("Please enter a number: "))
     except ValueError:
        print("Oops!  That was no valid number.  Try again...")
     
if(x>=0.9):
    print("A")
elif(x>=0.8):
    print("B")
elif(x>=0.7):
    print("C")
elif(x>=0.6):
    print("D")
elif(x<0.6):
    print("F")
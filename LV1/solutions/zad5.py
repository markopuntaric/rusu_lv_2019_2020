
fname = input('Enter the file name: ') 
try:
    fhand = open(fname)
except:
    print('File cannot be opened:', fname)
    exit()

x=0
sum=float(0)
count=int(0)

for line in fhand:
    words = line.split()
    
    for word in words:
        if(x==1):
            sum+=float(word)
            x=0
        if(word=="X-DSPAM-Confidence:"):
            count+=1
            x=1

print(float(sum/count))